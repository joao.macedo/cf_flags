#! /usr/bin/env python
#-*- coding: utf-8 -*-

from functools import reduce
from netCDF4 import Dataset
import numpy as np


def mask_cf_flags(ncvar, *meanings_filter):
    """Return a numpy.ndarray boolean mask object.
The filter rules will be constructed based on the 'meanings_filter' argument.

Parameters
----------
ncvar : netCDF4.Variable object
    The given netCDF4.Variable object must contain 'flag_meanings',
    'flag_values', and 'flag_masks' attributes as described in the
    CF-Conventions standard.

meanings_filter : List of Strings
    Unlimited number of 'meanings_filter' lists can be added.
    Each list should contain one or more "meaning values" to apply the AND
    operator,    resulting into an INTERCEPTION of the respective list
    "meaning values".
    The OR operator will be applied between lists, wich means that the final
    result will be the UNION of all the given 'meanings_filter' lists.

          meanings_filter_1                        meanings_filter_2

    [flag_meaning_1 AND flag_meaning_2] OR [flag_meaning_3 AND flag_meaning_4]

Returns
-------
a : numpy.ndarray
    A boolean mask array where all the pixels unmatching the given
    filter are masked (True).

Example
-------
Create a boolean mask wich the non masked pixels (False) will be the
UNION of 'goes' satellite 'sea_pixel' AND 'pixel_filled_by_clouds':

>>> import cf_flags

>>> fname = 'test/data/g2_BIOPAR_LST_201803141200_GLOBE_GEO_V1.2.nc'
>>> ncfid = Dataset(fname, 'r')
>>> ncvar = ncfid['Q_FLAGS']
>>> meanings_filter_1 = ['goes','sea_pixel']
>>> meanings_filter_2 = ['goes', 'pixel_filled_by_clouds']
>>> goes_sea_clowd_mask = cf_flags.get_masked_array(ncvar, meanings_filter_1, meanings_filter_2)
    """

    flags_dict = get_cf_flags(ncvar)

    mask = np.zeros(ncvar.shape, np.bool)
    for meanings in meanings_filter:
        and_mask = np.zeros(ncvar.shape, np.bool)
        for idx, meaning in enumerate(meanings):
            meaning_value = flags_dict[meaning]['value']
            meaning_mask = flags_dict[meaning]['mask']
            if idx == 0:
                and_mask = ((ncvar[...] & meaning_mask) == meaning_value)
            else:
                and_mask &= ((ncvar[...] & meaning_mask) == meaning_value)
        mask |= and_mask
    return ~mask


def get_cf_flags(ncvar):
    #flag_meanings = ncvar.getncattr('flag_meanings').split()
    flag_meanings = ncvar.getncattr('flag_meanings').replace(',','').split()

    # If flag_values attribute is missing we assume a
    # single bit mask for each meaning:
    if 'flag_values' in ncvar.ncattrs():
        flag_values = ncvar.getncattr('flag_values')
    else:
        flag_values = [1 for meaning in flag_meanings]

    # If flag_masks attribute is missing we assume a
    # unique mask for all meanings:
    if 'flag_masks' in ncvar.ncattrs():
        flag_masks = ncvar.getncattr('flag_masks')
    else:
        unique_mask = reduce(lambda x,y: x+2**y,
                             range(1 + int(np.log2(max(flag_values)))), 0)
        flag_masks = [unique_mask for meaning in flag_meanings]

    return dict([
        (meaning, {
            'value': flag_values[idx],
            'mask': flag_masks[idx],
            #'mask_bitshift': reduce(
            #    lambda x,y: x + (1 if (flag_masks[idx] & 2**y == 0) else 0),
            #    range(1 + int(np.log2(flag_masks[idx]))), 0
            #)
        })
        for idx, meaning in enumerate(flag_meanings)
    ])
